##Сборка проекта

* docker-compose build
* docker-compose up -d
* docker-compose exec backend
	* ./bin/console doctrine:migrations:migrate
	* ./bin/phpunit

* http://test-task/


#API
* GET /[en|ru]/book?page=1&perPage=10
* POST /book/create
	* name_ru string(32) require
	* name_en string(32) require
	* author array<int> require
* POST /author/create
	* name string(32) require
* GET /[en|ru]/book/{id}
	* id int require Book id
* GET /[en|ru]/book/search?query=
	* query string(32) require
