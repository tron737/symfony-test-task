<?php

namespace App\Tests;

use App\Entity\Book;
use App\Service\BookService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BookTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testGetBooks()
    {
        $bookService = new BookService($this->entityManager);
        $books = $bookService->getBooks(1, 10, 'ru');

        $this->assertIsArray($books);
    }

    public function testGetNameBook()
    {
        $bookService = new BookService($this->entityManager);

        $book = new Book();
        $book->setNameEn('test')->setNameRu('тест');

        $bookService->setBooktName($book, 'ru');

        $this->assertEquals('тест', $book->getName());
    }

    public function testCreateBook()
    {
        $bookService = new BookService($this->entityManager);
        $book = $bookService->saveBook('test', 'тест', ['тест']);

        $this->assertIsInt($book);
    }
}
