<?php


namespace App\Controller;


use App\Service\AuthorService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

class AuthorController extends AbstractFOSRestController
{
    /**
     * @var AuthorService $authorService
     */
    protected $authorService;

    /**
     * AuthorController constructor.
     * @param AuthorService $authorService
     */
    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    /**
     * @Rest\Get("/author", name="get_author")
     * @Rest\QueryParam(name="page", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="perPage", requirements="\d+", nullable=true)
     *
     * @param int|null $page
     * @param int|null $perPage
     * @return View
     */
    public function getAuthorAction(?int $page = 1, ?int $perPage = 10): View
    {
        return View::create($this->authorService->getAuthors($page ?? 1, $perPage ?? 10), 200);
    }

    /**
     * @Rest\Post("/author/create", name="create_author")
     * @Rest\RequestParam(name="name", requirements="\w{0,32}")
     *
     * @param string $name
     * @return View
     */
    public function postCreateAuthorAction(string $name): View
    {
        $authors = $this->authorService->saveAuthor([$name]);
        $author = current($authors);
        $authorId = $author->getId();
        return View::create((new \App\Dto\Response())->setId($authorId)->setSuccess(true), 200);
    }
}