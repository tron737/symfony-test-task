<?php


namespace App\Controller;

use App\Entity\Book;
use App\Service\AuthorService;
use App\Service\BookService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class BookController extends AbstractFOSRestController
{
    /**
     * @var BookService
     */
    private BookService $bookService;

    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }

    /**
     * @Rest\Get("/{_locale}/book", name="get_book")
     * @Rest\QueryParam(name="page", requirements="\d+", nullable=true, default="1")
     * @Rest\QueryParam(name="perPage", requirements="\d+", nullable=true, default="10")
     *
     * @param Request $request
     * @param int|null $page
     * @param int|null $perPage
     * @return View
     */
    public function getBooksAction(Request $request, ?int $page, ?int $perPage): View
    {
        return View::create($this->bookService->getBooks($page, $perPage, $request->getLocale()), 200);
    }

    /**
     * @Rest\Get("/{_locale}/book/{id}", requirements={"id" = "\d+"}, name="get_current_book")
     *
     * @param int $id
     * @param Request $request
     * @return View
     */
    public function getBookAction(int $id, Request $request): View
    {
        $book = $this->bookService->getBook($id, $request->getLocale());
        return View::create($book, 200);
    }

    /**
     * @Rest\Get("/{_locale}/book/search", name="get_search_book")
     *
     * @Rest\QueryParam(name="query",nullable=false)
     * @Rest\QueryParam(name="page", requirements="\d+", nullable=true, default="1")
     * @Rest\QueryParam(name="perPage", requirements="\d+", nullable=true, default="10")
     *
     * @param string $query
     * @param int|null $page
     * @param int|null $perPage
     * @param Request $request
     * @return View
     */
    public function getSearchBookAction(string $query, ?int $page, ?int $perPage, Request $request): View
    {
        return View::create($this->bookService->getBooks($page, $perPage, $request->getLocale(), $query), 200);
    }

    /**
     * @Rest\Post("/book/create")
     * @Rest\RequestParam(name="name_en", requirements="\w{0,32}")
     * @Rest\RequestParam(name="name_ru", requirements="\w{0,32}")
     * @Rest\RequestParam(name="author", requirements="\w{0,32}", map=true)
     *
     * @param string $name_en
     * @param string $name_ru
     * @param array $author
     * @return View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createBookAction(string $name_en, string $name_ru, array $author): View
    {
        $bookId = $this->bookService->saveBook($name_en, $name_ru, $author);
        return View::create((new \App\Dto\Response())->setId($bookId)->setSuccess(true), 200);
    }
}