<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="book")
 * @ORM\Entity(repositoryClass="App\Repository\BookRepository")
 */
class Book
{
    /**
     * @var int $id
     *
     * @Mapping\Column(name="id", type="bigint", unique=true)
     * @Mapping\Id()
     * @Mapping\GeneratedValue(strategy="IDENTITY")
     */
    protected int $id;

    /**
     * @var string $nameRu
     * @Mapping\Column(type="string", length=32, nullable=false)
     * @Serializer\Exclude()
     */
    protected string $nameRu;

    /**
     * @var string $nameEn
     * @Mapping\Column(type="string", length=32, nullable=false)
     * @Serializer\Exclude()
     */
    protected string $nameEn;

    protected string $name;

    /**
     * @ORM\ManyToMany(targetEntity=Author::class, inversedBy="books")
     */
    private $author;

    public function __construct()
    {
        $this->author = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameRu(): string
    {
        return $this->nameRu;
    }

    /**
     * @param string $nameRu
     * @return Book
     */
    public function setNameRu(string $nameRu): Book
    {
        $this->nameRu = $nameRu;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameEn(): string
    {
        return $this->nameEn;
    }

    /**
     * @param string $nameEn
     * @return Book
     */
    public function setNameEn(string $nameEn): Book
    {
        $this->nameEn = $nameEn;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Book
     */
    public function setName(string $name): Book
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Collection|Author[]
     */
    public function getAuthor(): Collection
    {
        return $this->author;
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->author->contains($author)) {
            $this->author[] = $author;
        }

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        $this->author->removeElement($author);

        return $this;
    }
}