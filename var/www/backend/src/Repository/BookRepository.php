<?php

namespace App\Repository;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    /**
     * @param int $page
     * @param int $perPage
     * @param string|null $query
     * @return Book[]
     */
    public function getBooks(int $page, int $perPage, ?string $query = null): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('t')
            ->from($this->getClassName(), 't')
            ->orderBy('t.id', 'DESC')
            ->setFirstResult($perPage * ($page - 1))
            ->setMaxResults($perPage);

        if ($query) {
            $queryBuilder
                ->where('t.nameRu like :search')
                ->orWhere('t.nameEn like :search')
                ->setParameter('search', '%' . $query . '%');
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param string $name_en
     * @param string $name_ru
     * @param Author[] $authors
     * @return Book
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createBook(string $name_en, string $name_ru, array $authors): Book
    {
        $book = new Book();
        $book->setNameEn($name_en)->setNameRu($name_ru);

        foreach ($authors as $author) {
            $book->addAuthor($author);
        }

        $this->getEntityManager()->persist($book);
        $this->getEntityManager()->flush();

        return $book;
    }
}