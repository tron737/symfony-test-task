<?php


namespace App\Service;


use App\Entity\Author;
use App\Entity\Book;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class BookService extends BaseService
{
    /**
     * @param string $name_en
     * @param string $name_ru
     * @param array $authors
     * @return int|null
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveBook(string $name_en, string $name_ru, array $authors): ?int
    {
        /** @var BookRepository $bookRepository */
        $bookRepository = $this->entityManager->getRepository(Book::class);
        /** @var AuthorRepository $authorRepository */
        $authorRepository = $this->entityManager->getRepository(Author::class);
        $authors = $authorRepository->findBy(['id' => $authors]);

        $book = $bookRepository->createBook($name_en, $name_ru, $authors);

        return $book->getId();
    }

    /**
     * @param int $page
     * @param int $perPage
     * @param string $locale
     * @param string|null $query
     * @return array
     */
    public function getBooks(int $page, int $perPage, string $locale, ?string $query = null): array
    {
        /** @var BookRepository $bookRepository */
        $bookRepository = $this->entityManager->getRepository(Book::class);
        $books = $bookRepository->getBooks($page, $perPage, $query);

        foreach ($books as $bookItem) {
            $this->setBookName($bookItem, $locale);
        }

        return $books;
    }

    /**
     * @param int $id
     * @param string $locale
     * @return Book|null
     */
    public function getBook(int $id, string $locale): ?Book
    {
        /** @var BookRepository $bookRepository */
        $bookRepository = $this->entityManager->getRepository(Book::class);
        /** @var Book $book */
        $book = $bookRepository->find($id);
        $this->setBookName($book, $locale);

        return $book;
    }

    /**
     * @param Book $book
     * @param $locale
     */
    public function setBookName(Book $book, $locale): void
    {
        $book->setName($book->{'getName' . ucfirst($locale)}());
    }
}