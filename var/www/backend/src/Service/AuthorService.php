<?php


namespace App\Service;


use App\Entity\Author;
use App\Repository\AuthorRepository;

class AuthorService extends BaseService
{
    public function saveAuthor(array $author): ?array
    {
        /** @var AuthorRepository $authorRepository */
        $authorRepository = $this->entityManager->getRepository(Author::class);
        return $authorRepository->createAuthor($author);
    }

    public function getAuthors(int $page, int $perPage): array
    {
        /** @var AuthorRepository $authorRepository */
        $authorRepository = $this->entityManager->getRepository(Author::class);
        return $authorRepository->getAuthors($page, $perPage);
    }
}